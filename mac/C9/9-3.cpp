#include <iostream>
using namespace std;

class singleton {
private:
    int count;
    singleton() {
        count = 0;
    }
    static singleton* instance;
public:
    static singleton *getInstance() {
        if(instance == 0) {
            instance = new singleton();
        }
        return instance;
    }
    void inc() {
        count++;
    }
    void print() {
        cout << count << endl;
    }
};

singleton *singleton::instance = 0;

int main() {
    singleton *p;
    p = singleton::getInstance();
    p->inc();
    p->inc();
    p->print();
    singleton *p2;
    p2 = singleton::getInstance();
    p2->inc();
    p2->inc();
    p2->print();
    return 0;
}