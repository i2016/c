#include <iostream>
#include <cmath>
using namespace std;

class figure {
public:
    virtual void scale(float s) = 0;
    virtual float menseki() = 0;
    virtual void print() {
        cout << menseki() << endl;
    }
};

class rectangular: public figure {
public:
    float tate, yoko;
    rectangular(float t, float y) {
        tate = t;
        yoko = y;
    }
    rectangular(float t) {
        tate = yoko = t;
    }
    float menseki() {
        return tate * yoko;
    }
    void scale(float s) {
        tate *= s;
        yoko *= s;
    }
};
class triangle : public figure {
public:
        float a;
        float b;
        float c;
        triangle(float a1, float b1, float c1) {
                a = a1;
                b = b1;
                c = c1;
        }
        float menseki() {
                float s = (a + b + c) / 2;
                return sqrt(s * (s - a) * (s - b) * (s - c));
        }
        void scale(float s) {
            a *= s;
            b *= s;
            c *= s;
        }
};

int main() {
    float a, b, c;
    float s;
    cin >> a >> b >> c;
    rectangular r1(a, b);
    r1.print();
    rectangular r2(a);
    r2.print();
    triangle t(a, b, c);
    t.print();
    cin >> s;
    r1.scale(s);
    r1.print();
    r2.scale(s);
    r2.print();
    t.scale(s);
    t.print();
    return 0;   
}