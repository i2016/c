#include <iostream>
#include <cmath>
using namespace std;

class figure {
public:
    virtual float menseki() = 0;
    virtual void print() {
        cout << menseki() << endl;
    }
};
class rectangular: public figure {
public:
    float tate, yoko;
    rectangular(float t, float y) {
        tate = t;
        yoko = y;
    }
    rectangular(float t) {
        tate = yoko = t;
    }
    float menseki() {
        return tate * yoko;
    }
};
class triangle: public figure {
public:
    float a, b, c;
    triangle(float a1, float b1, float c1) {
        a = a1;
        b = b1;
        c = c1;
    }
    float menseki() {
        float s = (a + b + c) / 2;
        return sqrt(s * (s - a) * (s - b) * (s - c));
    }
};
int main() {
    float a, b, c;
    cin >> a >> b >> c;
    rectangular r1(a);
    r1.print();
    rectangular r2(a, b);
    r2.print();
    triangle t(a, b, c);
    t.print();
    return 0; 
}