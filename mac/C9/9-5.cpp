#include <iostream>
using namespace std;

class Fib {
private:
    static int count;
public:
    static int get() {
        return count;
    }
    int fib(int n) {
        count++;
        if(n == 0) {
            return 0;
        } else if(n == 1) {
            return 1;
        } else {
            return fib(n - 1) + fib(n - 2);
        }
    }
};
int Fib::count = 0;
int main() {
    Fib f; int n;
    cin >> n;
    cout << f.fib(n) << ": ";
    cout << f.get() << endl;
    Fib f2;
    cin >> n;
    cout << f2.fib(n) << ": ";
    cout << Fib::get() << endl;
    return 0;
}