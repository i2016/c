#include <iostream>
using namespace std;

class Fact {
private: 
    static int count;
public:
    static int get() {
        return count;
    }
    int fact(int n) {
        count++;
        if(n == 0) {
            return 1;
        } else {
            return n * fact(n-1);
        }
    }
};
int Fact::count = 0;

int main() {
    Fact f;
	int n1;
	cin >> n1;
	cout << f.fact(n1) << ": ";
	cout << f.get() << endl;
	int n2;
	cin >> n2;
	cout << f.fact(n2) << ": ";
	cout << f.get() << endl;
	Fact f2;
	cout <<	f2.fact(n1) << ": ";
	cout << f2.get() << endl;
	cout <<	f2.fact(n2) << ": ";
	cout << f2.get() << endl;
	cout << Fact::get() << endl;
	return 0;
}