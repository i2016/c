#include <iostream>
using namespace std;

class date {
protected:
    int hour, minute;
public:
    date(int h, int m) {
        int t = h * 60 + m;
        hour = t / 60;
        minute = t % 60;
    }
    void print() {
        cout << hour << "時間" << minute << "分" << endl;
    }
    date operator+(date a) {
        return date(hour + a.hour, minute + a.minute);
    }
    date operator*(int n) {
        int t = (hour * 60 + minute) * n;
        return date(t / 60, t % 60);
    }
};
date operator*(int n, date a) {
    return a * n;
}

class date2: public date {
private:
    int second;
public:
    date2(int h, int m, int s) : date(h, m + s / 60) {
        second = s % 60;
    }
    date2 operator+(date2 a) {
        return date2(hour + a.hour, minute + a.minute, second + a.second);
    }
    date2 operator*(int n) {
        return date2(hour * n, minute * n, second * n);
    }
    void print() {
        cout << hour << "時間" << minute << "分" << second << "秒" << endl;
    }
};
date2 operator*(int n, date2 a) {
    return a * n;
}
int main() {
    int h, m, s;
    cin >> h >> m >> s;
    date2 a(h, m, s);
    a.print();
    int n;
    cin >> n;
    a = a * n;
    a.print();
    a = n * a;
    a.print();
    return 0;
}

