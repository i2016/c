#include <iostream>
using namespace std;

class Frac {
protected:
    int bunshi, bunbo;
    int gcd(int m, int n) {
        int p;
        for(p=(m>n)?m:n; p >= 1; p--) {
            if(m%p==0 && n%p==0) break;
        }
        return p;
    }
public:
    Frac(int shi, int bo) {
        int p = gcd(shi, bo);
        bunshi = shi/p;
        bunbo = bo/p;
    }
    void print() {
        if(bunbo == 1) {
            cout << bunshi << endl;
        } else if(bunshi > bunbo) {
            cout << bunshi/bunbo << " + (" << bunshi % bunbo << "/" << bunbo << ")" << endl;
        } else {
            cout << "(" << bunshi << "/" << bunbo << ")" << endl;
        }
    }
    virtual Frac operator*(Frac b) {
        int shi = bunshi * b.bunshi;
        int bo = bunbo * b.bunbo;
        return Frac(shi, bo);
    }
    virtual Frac operator/(Frac b) {
        int shi = bunshi * b.bunbo;
        int bo = bunbo * b.bunshi;
        return Frac(shi, bo);
    }
    virtual Frac operator+(Frac b) {
        int shi = bunshi * b.bunbo + b.bunshi * bunbo;
        int bo = bunbo * b.bunbo;
        return Frac(shi, bo);
    }
    virtual Frac operator-(Frac b) {
        int shi = bunshi * b.bunbo - b.bunshi * bunbo;
        int bo = bunbo * b.bunbo;
        return Frac(shi, bo);
    }
};
class Frac2: public Frac {
public:
    Frac2(int shi, int bo) : Frac(shi, bo) {
    }
    Frac2 operator*(int n) {
            return Frac2(bunshi * n, bunbo);
    }
    Frac2 operator/(int n) {
        return Frac2(bunshi, bunbo * n);
    }
};

int main() {
    int a, b;
    cin >> a >> b;
    Frac2 f1 = Frac2(a, b);
    f1.print();
    int n;
    cin >> n;
    Frac2 f2 = f1 * n;
    f2.print();
    f2 = f1 / n;
    f2.print();
    return 0;
}