#include <iostream>
using namespace std;

class Frac {
protected:
    int bunshi, bunbo;
    int gcd(int m, int n) {
        int p;
        for(p=(m>n)?m:n; p >= 1; p--) {
            if(m%p==0 && n%p==0) break;
        }
        return p;
    }
public:
    Frac(int shi, int bo) {
        int p = gcd(shi, bo);
        bunshi = shi/p;
        bunbo = bo/p;
    }
    void print() {
        if(bunbo == 1) {
            cout << bunshi << endl;
        } else if(bunshi > bunbo) {
            cout << bunshi / bunbo << " + (" << bunshi % bunbo << "/" << bunbo << ")" << endl;
        } else {
            cout << "(" << bunshi << "/" << bunbo << ")" << endl;
        }
    }
    virtual Frac operator*(Frac b) {
        int shi = bunshi * b.bunshi;
        int bo = bunbo * b.bunbo;
        return Frac(shi, bo);
    }
    virtual Frac operator/(Frac b) {
        int shi = bunshi * b.bunbo;
        int bo = bunbo * b.bunshi;
        return Frac(shi, bo);
    }
    virtual Frac operator+(Frac b) {
        int shi = bunshi * b.bunbo + b.bunshi * bunbo;
        int bo = bunbo * b.bunbo;
        return Frac(shi, bo);
    }
    virtual Frac operator-(Frac b) {
        int shi = bunshi * b.bunbo - b.bunshi * bunbo;
        int bo = bunbo * b.bunbo;
        return Frac(shi, bo);
    }
};
int main() {
    int h, m;
    cin >> h >> m;
    Frac a1 = Frac(h, m);
    a1.print();
    cin >> h >> m;
    Frac a2 = Frac(h, m);
    a2.print();
    Frac b = a1 * a2;
    b.print();
    b = a1 / a2; 
    b.print();
    b = a1 + a2;
    b.print();
    b = a1 - a2;
    b.print(); 
    return 0;
}