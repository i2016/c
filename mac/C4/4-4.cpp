#include <iostream>
using namespace std;
class Date {
private:
    int year;
    int month;
    int day;
public:
    Date(int y, int m, int d) {
        int cm[] = {0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
        if (!(1 <= y) || (!(1 <= m && m <= 12)) || (!(1 <= d && d <= cm[m])) ) {
            year = month = day = 0;
        } else {
            year = y;
            month = m;
            day = d;
        }
    }
    void get(int &y, int &m, int &d) {
        y = year;
        m = month;
        d = day;
    }
    void next() {
        int cm[] = {0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
        day++;
        if (cm[month] < day) {
            day = 1;
            month++;
            if (12 < month) {
                month = 1;
                year++;
            }
        }
    }
};


int main() {
    int yy, mm, dd, y, m, d; cin >> yy >> mm >> dd;
    Date d1(yy, mm, dd);
    d1.get(y, m, d);
    if (y != 0 && m != 0 && d != 0) {
        cout << yy << "年" << mm << "月" << dd << "日か設定されました" << endl;
        d1.next();
        d1.get(yy, mm, dd);
        cout << "次の日は" << yy << "年" << mm << "月" << dd << "日てす" << endl;
    } else {
        cout << "正しく設定されていません" << endl;
    }
    return 0;
}
