#include <iostream>
using namespace std;
class Date {
private:
    int year;
    int month;
    int day;
public:
    Date(int y, int m, int d) {
        int cm[] = {0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
        if (!(1 <= y) || (!(1 <= m && m <= 12)) || (!(1 <= d && d <= cm[m])) ) {
            year = month = day = 0;
        } else {
            year = y;
            month = m;
            day = d;
        }
    }
    void get(int &y, int &m, int &d) {
        y = year;
        m = month;
        d = day;
    }
    void next() {
        int cm[] = {0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
        day++;
        if (cm[month] < day) {
            day = 1;
            month++;
            if (12 < month) {
                month = 1;
                year++;
            }
        }
    }
    int sub(Date d) {
        int y2, d2, m2, x;
        d.get(y2, m2, d2);
        for (x = 0; !(year == y2 && month == m2 && day == d2); x++) {
            d.next();
            d.get(y2, m2, d2);
        }
        return x;
    }
};


int main() {
    int yy, mm, dd;
    cin >> yy >> mm >> dd;
    Date d1(yy, mm, dd);
    d1.get(yy, mm, dd);
    if (yy == 0 && mm == 0 && dd == 0) {
        cout << "正しく設定されていません" << endl;
        return 1;
    }
    cin >> yy >> mm >> dd;
    Date d2(yy, mm, dd);
    d2.get(yy, mm, dd);
    if (yy == 0 && mm == 0 && dd == 0) {
        cout << "正しく設定されていません" << endl;
        return 1;
    }
    d1.get(yy, mm, dd);
    cout << yy << "年" << mm << "月" << dd << "日から"; d2.get(yy, mm, dd);
    cout << yy << "年" << mm << "月" << dd << "日まては"; cout << d2.sub(d1) << "日てす" << endl;
    return 0;
}
