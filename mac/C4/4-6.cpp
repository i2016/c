#include <iostream>
#include <cmath>
using namespace std;
class Kukei {
private:
    double x;
    double y;
    double w;
public:
    Kukei() {
        x = y = 0;
        w = 10;
    }
    Kukei(double xx, double yy, double ww) {
        x = xx;
        y = yy;
        w = ww;
    }

    bool isOverlap(Kukei o) {
        if((x <= o.x && o.x <= x + w) && (y <= o.y && o.y <= y + w)) {
            return true;
        } else if((x <= o.x + o.w && o.x + o.w <= x + w) && (y <= o.y && o.y <= y + w)) {
            return true;
        } else if((x <= o.x && o.x <= x + w) && (y <= o.y + o.w && o.y + o.w <= y + w)) {
            return true;
        } else if((x <= o.x + o.w && o.x + o.w <= x + w) && (y <= o.y + o.w && o.y + o.w <= y + w)) {
            return true;
        } else if((o.x <= x && x <= o.x + o.w) && (o.y <= y && y <= o.y + o.w)) {
            return true;
        } else if((o.x <= x + w && x + w <= o.x + o.w) && (o.y <= y && y <= o.y + o.w)) {
            return true;
        } else if((o.x <= x && x <= o.x + o.w) && (o.y <= y + w && y + w <= o.y + o.w)) {
            return true;
        } else if((o.x <= x + w && x + w <= o.x + o.w) && (o.y <= y + w && y + w <= o.y + o.w)) {
            return true;
        }
        return false;
    }

    bool isOverlapR(Kukei o) {
        double x1, y1, x2, y2;
        x1 = (x + x + w) / 2;
        y1 = (y + y + w) / 2;
        x2 = (o.x + o.x + o.w) / 2;
        y2 = (o.y + o.y + o.w) / 2;
        if(sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1)) <= (w /2 + o.w / 2)) {
            return true;
        }
        return false;
    }

};

int main() {
    Kukei a;
    double x, y, w;
    cin >> x >> y >> w;
    Kukei b(x, y, w);
    cin >> x >> y >> w;
    Kukei c(x, y, w);
    if(a.isOverlap(b)) {
        cout << "aとbは矩形か接しています" << endl;
    }
    if(b.isOverlap(c)) {
        cout << "bとcは矩形か接しています" << endl;
    }if(c.isOverlap(a)) {
        cout << "cとaは矩形か接しています" << endl;
    }
    if(a.isOverlapR(b)) {
        cout << "aとbは内接円か接しています" << endl;
    }
    if(b.isOverlapR(c)) {
        cout << "bとcは内接円か接しています" << endl;
    }
    if(c.isOverlapR(a)) {
        cout << "cとaは内接円か接しています" << endl;
    }
    return 0;
}