#include <iostream>
using namespace std;
class Frac {
private:
    int bunshi;
    int bunbo;
    int GCD(int n1, int n2) {
        for(int i=min(n1, n2); i>=1; i--) {
            if (n1 % i == 0 && n2 % i == 0){
                return i;
            }
        }

        return 1;
    }
public:
    Frac(int n) {
        bunshi = n;
        bunbo = 1;
    }
    Frac(int num, int den) {
        int n = GCD(num, den);
        bunshi = num / n;
        bunbo = den / n;
    }
    Frac mul(Frac a) {
        int num = bunshi * a.bunshi;
        int den = bunbo * a.bunbo;
        return Frac(num, den);
    }
    Frac div(Frac a) {
        int num = bunshi * a.bunbo;
        int den = bunbo * a.bunshi;
        return Frac(num, den);
    }
    Frac add(Frac a) {
        int num = bunshi * a.bunbo + a.bunshi * bunbo;
        int den = bunbo * a.bunbo;
        return Frac(num, den);
    }
    Frac sub(Frac a) {
        int num = bunshi * a.bunbo - a.bunshi * bunbo;
        int den = bunbo * a.bunbo;
        return Frac(num, den);
    }
    void print() {
        if(bunbo == 1) {
            cout << bunshi << endl;
        } else {
            cout << bunshi << " / " << bunbo << endl;
        }
    }
};

int main() {
    int seisuu;
    cin >> seisuu; Frac a(seisuu);
    a.print();
    int bunshi, bunbo;
    cin >> bunshi >> bunbo;
    Frac b(bunshi, bunbo);
    b.print();
    a.mul(b).print();
    a.add(b).print();
    a.sub(b).print();
    a.div(b).print();
    a.add(b).mul(b).print();
    return 0;
}