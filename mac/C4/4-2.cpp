#include <iostream>
using namespace std;
class que {
private:
    int idx;
    int buf[10];
public:
    que() {
        idx = 0;
    }
    void push(int v) {
        buf[idx++] = v;
    }
    int pop() {
        int top = buf[0];
        for (int i = 1; i < idx; i++) {
            buf[i-1] = buf[i];
        }
        idx--;
        return top;
    }
};
int main() {
    que a, b;
    int n, v;
    cin >> n;
    for (int i = 0; i < n; i++) {
        cin >> v; a.push(v);
        cin >> v; b.push(v); 
    }
    for (int i = 0; i < n; i++) {
        cout << a.pop() << endl;
        cout << b.pop() << endl;
    }
    return 0;
}
