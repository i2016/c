#include <iostream>
using namespace std;
int idx;
int buf[10];
void empty() {
    idx = 0;
}
void push(int v) {
    buf[idx++] = v;
}
int pop() {
    int top = buf[0];
    for (int i=1; i<idx; i++) {
        buf[i-1] = buf[i];    
    }
    idx--;
    return top;
}
int main() {
    empty();
    int v;
    for (int i=0; i<10; i++) {
        cin >> v;
        push(v);
    }
    for (int i=0; i<10; i++) {
        cout << pop() << " ";
    }
    for (int i=0; i<10; i++) {
        cin >> v;
        push(v);
    }
    for (int i=0; i<10; i++) {
        cout << pop() << " ";
    }
    cout << endl;
    return 0;
}