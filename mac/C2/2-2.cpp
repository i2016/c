#include <iostream>
using namespace std;
int main(void) {
	double kg,BMI=22;
	for (double i = 150; i <= 170; i += 5) {
        double j;
        j = i / 100.0;
        kg = BMI*j*j;
		cout.width(6);
        cout.precision(4);
        cout << left << j;
		cout.width(5);
		cout.precision(4);
        cout << kg << endl;
	}
    return 0;
}