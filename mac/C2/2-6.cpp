#include <iostream>
using namespace std;
int idx = 0;
int buf[10];
void empty(void) {
    idx = 0; 
}
void push(int v) {
    buf[idx++] = v;
}
int pop(void) {
    return buf[--idx];
}
int main() {
    empty();
    int n;
    cin >> n;
    for (int i=0; i<n; i++) {
        int v;
        cin >> v;
        push(v);
    }
    for (int i=0; i<n; i++) {
        cout << pop() << endl;
    }
    return 0; 
}