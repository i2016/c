#include <iostream>
using namespace std;
int main() {
    cout << "   |";
    for (int x=1; x<=9; x++) {
        cout.width(3);
        cout << x;
    }
    cout << endl;
    cout << "---+";
    for (int x=1; x<=9; x++) {
        cout << "---";
    }
    cout << endl;
    for (int y=1; y<=9; y++) {
        cout.width(3);
        cout << y << "|";
        for (int x=1; x<=9; x++) {
            cout.width(3);
            cout << x * y;
        }
        cout << endl;
    }
return 0; }