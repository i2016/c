#include <iostream>
using namespace std;
int chouhouS(int tate, int yoko) {
    int menseki;
    menseki = 2 * (tate + yoko);
    return menseki;
}
int seihouS(int ippen) {
    return chouhouS(ippen, ippen);
}
int main() {
    int a;
    cin >> a;
    cout << seihouS(a) << endl;
    int b;
    cin >> a >> b;
    cout << chouhouS(a, b) << endl;
    return 0;
}