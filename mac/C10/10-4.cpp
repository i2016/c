#include <iostream>
#include <algorithm>
#include <vector>
using namespace std;
int main() {
    vector<int> a;
    int v;

    while(cin >> v) {
        a.push_back(v);
    }
    sort(a.begin(), a.end());
    for(int i = 0; i < a.size(); i++) {
        cout << a[i] << " ";
    }
    cout << endl;
    return 0;
}