#include <iostream>
using namespace std;

template <class T> class que {
private:
    int idx;
    T buf[10];
public:
    que() {
        idx = 0;
    }
    void push(T v) {
        buf[idx++] = v;
    }
    T pop() {
        T top = buf[0];
        for(int i = 1; i < idx; i++) {
            buf[i-1] = buf[i];
        }
        idx--;
        return top;
    }
    bool isEmpty() {
        return idx == 0 ? true : false;
    }
};

int main() {
    que<int> a;
    int v;
    while (cin >> v && v != 0) {
        a.push(v);
    }
    while (!a.isEmpty()) {
        cout << a.pop() << " ";
    }
    cout << endl;

    que<double> b;
    double w;
    while (cin >> w && w != 0) {
        b.push(w);
    }
    while (!b.isEmpty()) {
        cout << b.pop() << " ";
    }
    cout << endl;
    return 0;
}