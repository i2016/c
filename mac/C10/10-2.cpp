#include <iostream>
using namespace std;
template <class T> class Stack {
    int idx;
    T buf[10];
public:
    Stack(void) {
        idx = 0;
    }
    void push(T v) {
        buf[idx++] = v;
    }
    T pop(void) {
        return buf[--idx];
    }
    bool isEmpty() {
        return idx == 0 ? true : false;
    }
};
int main() {
    Stack<int> s1;
    Stack<double> s2;
    int a;
    double b;
    while (cin >> a && a != 0) {
        s1.push(a);
    }
    while (!s1.isEmpty()) {
        cout << s1.pop() << " ";
    }
    cout << endl;
    while (cin >> b && b != 0) {
        s2.push(b);
    }
    while (!s2.isEmpty()) {
        cout << s2.pop() << " ";
    }
    cout << endl;
    return 0;
}