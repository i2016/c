#include <iostream>
using namespace std;
#define ARRAY_LEN(a) (sizeof(a) / sizeof((a)[0]))
template <class T> T median(T a1, T a2, T a3, T a4, T a5) {
    T a[5];
    a[0] = a1;
    a[1] = a2;
    a[2] = a3;
    a[3] = a4;
    a[4] = a5;
    for(int i=0; i<ARRAY_LEN(a); i++) {
        for(int j=0; j<ARRAY_LEN(a)-1; j++) {
            if(a[j] > a[j+1]) {
                T t = a[j];
                a[j] = a[j+1];
                a[j+1] = t;
            }
        }
    }
    return a[2];
}
int main() {
    int a1, a2, a3, a4, a5;
    cin >> a1 >> a2 >> a3 >> a4 >> a5;
    cout << median(a1, a2, a3, a4, a5) << endl;
    double b1, b2, b3, b4, b5;
    cin >> b1 >> b2 >> b3 >> b4 >> b5;
    cout << median(b1, b2, b3, b4, b5) << endl;
    return 0;
}