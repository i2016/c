#include <iostream>
#include <vector>
using namespace std;

vector<int> a(10);
void print() {
    for(int i = 0; i< a.size(); i++) {
        cout << a[i] << " ";
    }
    cout << endl;
}
void put(int b, int n) {
    for(int i = 0; i < n; i++) {
        a[i] = b*i;
    }
    print();
}

int main() {
    put(2, 10);
    a.resize(20);
    put(3, 20);
    a.resize(5);
    a.push_back(1);
    print();
    return 0;
}