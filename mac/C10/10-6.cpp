#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

class Median {
private:
    vector<int> a;
public:
    void push_back(int v) {
        a.push_back(v);
    }
    double get() {
        sort(a.begin(), a.end());
        int len = a.size();
        if (len % 2 == 0) {
            return (a[len / 2 - 1] + a[len / 2]) / 2.0;
        } else {
            return a[len / 2];
        }
    }
};

int main() {
    Median m;
    int a;
    while (cin >> a) {
    m.push_back(a); }
    cout << m.get() << endl;
    return 0; 
}