#include <iostream>
using namespace std;
int idx;
int buf[10];
void empty(void) {
    idx = 0;
}
void push(int v) {
    buf[idx++] = v; 
}
int pop(void) {
    return buf[--idx];
}
int main() {
    empty(); 
    int n, v;
    cin >> n; 
    for (int i = 0; i < n; i++) {
        cin >> v;
        push(v);
    }
    for (int i = 0; i < n; i++) {
        cout << pop() << endl;
    }
    return 0;
}