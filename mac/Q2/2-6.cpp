#include <iostream>
#include <cmath>
using namespace std;
double helon(double a, double b, double c) {
    float s = (a + b + c) / 2;
    return sqrt(s * (s - a) * (s - b) * (s - c));
}
int main() {
    double a,b,c;
    cin >> a >> b >> c;
    cout << helon(a,b,c) << endl;
    return 0;
}