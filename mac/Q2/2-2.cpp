#include <iostream>
using namespace std;
int buf=0;
void inc() {
    buf++;
}
void dec() {
    buf--;
}

int main() {
    buf = 999;
    inc();
    cout << buf << endl; 
    dec();
    cout << buf << endl;
    return 0;
}