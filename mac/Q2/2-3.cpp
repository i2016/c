#include <iostream>
using namespace std;
int idx;
int buf[10];
void empty(void) {
    idx = 0;
}
void push(int v) {
    buf[idx++] = v; 
}
int pop(void) {
    return buf[--idx];
}
int main() {
    empty();
    push(100);
    push(200);
    cout << pop() << endl;
    cout << pop() << endl;
    return 0;
}