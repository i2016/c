#include <iostream>
using namespace std;
int main() {
    cout.width(5);
    cout << right << 25 <<endl;

    cout.width(3);
    cout.fill('0');
    cout << 25 <<endl;

    cout.width(5);
    cout.precision(3);
    cout.fill('0');
    cout << 15.32 << endl;

    cout.width(6);
    cout.fill(' ');
    cout << right << "melon" << endl;

    return 0;
}