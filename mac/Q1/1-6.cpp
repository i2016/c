#include <iostream> 
using namespace std;
int main() {
    for(int i=150; i<=170; i+=5) {
        double h; 
        h = i/100;
        cout.width(6);
        cout.precision(4);
        cout << left << (double)i/100;
        cout << "  ";
        cout.width(5);
		cout.precision(4);
        cout << 22*(double)i/100*(double)i/100 << endl;
    }
    return 0;
}