#include <iostream>
using namespace std;
int main() {
    int x;
    cout << "n?: ";
    cin >> x;
    cout << "   |";
    for(int i=1; i<=x; i++) {
        cout.width(3);
        cout << i;
    }
    cout << endl;
    cout << "---+";
    for(int i=1; i<=x;i++) {
        cout << "----";
    }
    cout <<endl;
    for (int y = 1; y<=x; y++) {
        cout.width(3);
        cout << y << "|";
        for (int z = 1; z<=x; z++) {
            cout.width(3);
            cout << z * y;
        }
        cout << endl;
    }
    return 0;
}