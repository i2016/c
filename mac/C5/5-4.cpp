#include <iostream>
using namespace std;
double shuui(double r) {
	return 2 * r * 3.14;
}
double shuui(double w, double h) {
	return (w + h) * 2;
}
double shuui(double a, double b, double c) {
	return a + b + c;
}
int main() {
	double r;

	cin >> r;
	cout << shuui(r) << endl;

	double tate, yoko;
	cin >> tate >> yoko;
	cout << shuui(tate, yoko) << endl;

	double a, b, c;
	cin >> a >> b >> c;
	cout << shuui(a, b, c) << endl;

	return 0;
}