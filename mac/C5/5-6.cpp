#include <iostream>
using namespace std;

class que {
protected:
    int idx;
    int buf[10];
public:
    que() {
        idx = 0;
    }
    void push(int v) {
        buf[idx++] = v;
    }
    int pop() {
        int top = buf[0];
        for (int i = 1; i < idx; i++) {
            buf[i-1] = buf[i];
        }
        idx--;
        return top;
    }
};
class que2: public que {
public:
    void swap() {
        if(idx>=2) {
            int top = buf[0];
            buf[0] =buf[1];
            buf[1] = top;
        }
    }
};
int main() {
	que2 a;
	int n, v;

	cin >> n;
	for(int i = 0; i < n; i++){
		cin >> v;
		a.push(v);
	}
	a.swap();
	for(int i = 0; i < n; i++){
		cout << a.pop() << " ";
	}
	cout << endl;
	return 0;
}