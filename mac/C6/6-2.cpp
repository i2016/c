#include <iostream>
using namespace std;
class MedicalCheck {
protected:
    int ID;
    double height;
    double weight;
public:
    void setID(int id) {
        ID = id;
    }
    int getID() {
        return ID;
    }
    void setHeight(double h) {
        height = h;
    }
    double setHeight() {
        return height;
    }
    void setWeight(double w) {
        weight = w;
    }
    double setWeight() {
        return weight;
    }
    double BMI() {
        return weight/(height*height);
    }
    double StandardBodyWeight() {
        return 22 * height * height;
    }
};
class MedicalCheck2: public MedicalCheck {
public:
    double rohrer() {
                return weight/(height*height*height)*10;
    }
    double StandardBodyWeight() {
        return 130*(height*height*height)/10;
    }
};

int main() {
    int id;
    double h, w;

    cin >> id >> h >> w;
    MedicalCheck mc;
    mc.setID(id);
    mc.setHeight(h);
    mc.setWeight(w);
    cout << mc.BMI() << endl;
    cout << mc.StandardBodyWeight() << endl;

    MedicalCheck2 mc2;
    mc2.setID(id);
    mc2.setHeight(h);
    mc2.setWeight(w);
    cout << mc2.rohrer() << endl;
    cout << mc2.StandardBodyWeight() << endl;
    return 0;
}