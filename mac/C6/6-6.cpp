#include <iostream>
using namespace std;
class Insurance
{
  private:
    int age;
    int color;

  public:
    void setAge(int a)
    {
        age = a;
    }
    int getAge()
    {
        return age;
    }
    void setColor(int c)
    {
        color = c;
    }
    int getColor()
    {
        return color;
    }
    int fee()
    {
        int x = 0;
        switch (color)
        {
        case 0:
            x = 6000;
            break;
        case 1:
            x = 7000;
            break;
        case 2:
            x = 5000;
            break;
        }
        return x;
    }
};
class Insurance2 : public Insurance
{
  public:
    int fee()
    {
        int c = getColor();
        int a = getAge();
        if (c == 0 || c == 1)
        {
            if (a <= 29)
            {
                return 8000;
            }
            else if (a <= 49)
            {
                return 6500;
            }
            else
            {
                return 8500;
            }
        }
        else
        {
            if (a <= 29)
            {
                return 5000;
            }
            else if (a <= 49)
            {
                return 3500;
            }
            else
            {
                return 4500;
            }
        }
    }
};

int main()
{
    int a, c;
    cin >> a >> c;
    Insurance i1;
    i1.setAge(a);
    i1.setColor(c);
    cout << i1.fee() << endl;
    Insurance2 i2;
    i2.setAge(a);
    i2.setColor(c);
    cout << i2.fee() << endl;
    return 0;
}