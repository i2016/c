#include <iostream>
using namespace std;
class MedicalCheck {
protected:
    int ID;
    double height;
    double weight;
public:
    void setID(int id) {
        ID = id;
    }
    int getID() {
        return ID;
    }
    void setHeight(double h) {
        height = h;
    }
    double setHeight() {
        return height;
    }
    void setWeight(double w) {
        weight = w;
    }
    double setWeight() {
        return weight;
    }
    double BMI() {
        return weight/(height*height);
    }
    double StandardBodyWeight() {
        return 22 * height * height;
    }
};

int main() {
    int id;
    double h, w;
    cin >> id >> h >> w;
    MedicalCheck mc;
    mc.setID(id);
    mc.setHeight(h);
    mc.setWeight(w);
    cout << mc.BMI() << endl;
    cout << mc.StandardBodyWeight() << endl;
    return 0;
}