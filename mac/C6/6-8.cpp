#include <iostream>
using namespace std;
class que
{
  protected:
    int buf[10];
    int idx;

  public:
    que()
    {
        idx = 0;
    }
    int pop()
    {
        int top = buf[0];
        for (int i = 1; i < idx; i++)
        {
            buf[i - 1] = buf[i];
        }
        idx--;
        return top;
    }
    void push(int v)
    {
        //cout << "C" << endl;
        buf[idx++] = v;
    }
};
class que2 : public que
{
  public:
    void push(int v)
    {
        if (v > 0)
        {
            que::push(v);
        }
    }
    int isEmpty()
    {
        static int count = 0;
        if (count < idx)
            return 0;
        else
            return 1; //空
    }
};
int main()
{
    int n;
    que q1;
    que2 q2;

    cin >> n;
    for (int i = 0; i < n; i++)
    {
        int v;
        cin >> v;
        q1.push(v);
        q2.push(v);
    }

    for (int i = 0; i < n; i++)
    {
        cout << q1.pop() << " ";
    }
    cout << endl;

    while (q2.isEmpty() == false)
    {
        cout << q2.pop() << " ";
    }
    cout << endl;

    return 0;
}