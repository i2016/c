#include <iostream>
using namespace std;
class MedicalCheck
{
  protected:
    int ID;
    double height;
    double weight;

  public:
    void setID(int id)
    {
        ID = id;
    }
    int getID()
    {
        return ID;
    }
    void setHeight(double h)
    {
        height = h;
    }
    double setHeight()
    {
        return height;
    }
    void setWeight(double w)
    {
        weight = w;
    }
    double setWeight()
    {
        return weight;
    }
    double BMI()
    {
        return weight / (height * height);
    }
    double StandardBodyWeight()
    {
        return 22 * height * height;
    }
};
class MedicalCheck2 : public MedicalCheck
{
  public:
    double rohrer()
    {
        return weight / (height * height * height) * 10;
    }
    double StandardBodyWeight()
    {
        return 130 * height * height * height / 10;
    }
};
class MedicalCheck3 : public MedicalCheck2
{
  private:
    int age;

  public:
    void setAge(int a)
    {
        age = a;
    }
    int getAge()
    {
        return age;
    }
    double StandardBodyWeight()
    {
        if (age < 15)
        {
            return MedicalCheck2::StandardBodyWeight();
        }
        else
        {
            return MedicalCheck::StandardBodyWeight();
        }
    }
};

int main()
{
    int id, a;
    double h, w;
    cin >> id >> h >> w >> a;
    MedicalCheck3 mc;
    mc.setID(id);
    mc.setHeight(h);
    mc.setWeight(w);
    mc.setAge(a);
    cout << mc.BMI() << endl;
    cout << mc.rohrer() << endl;
    cout << mc.StandardBodyWeight() << endl;
    return 0;
}