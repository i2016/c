#include <iostream>
#define N 10
using namespace std;

class que
{
  protected:
    int buf[N];
    int idx;

  public:
    que()
    {
        idx = 0;
    }
    void push(int v)
    {
        buf[idx++] = v;
    }

    int pop()
    {
        int top = buf[0];
        for (int i = 1; i < idx; i++)
        {
            buf[i - 1] = buf[i];
        }
        idx--;
        return top;
    }
};

class pque : public que
{
  public:
    int pop()
    {
        int j = idx, min = buf[0], box;
        for (int i = 1; i < j; i++)
        {
            if (min > buf[i])
            {
                box = min;
                min = buf[i];
                buf[i] = box;
            }
        }
        for (int i = 1; i < idx; i++)
        {
            buf[i - 1] = buf[i];
        }
        idx--;
        return min;
    }
};

int main()
{
    que q1;
    pque q2;
    int n;
    cin >> n;
    for (int i = 0; i < n; i++)
    {
        int v;
        cin >> v;
        q1.push(v);
        q2.push(v);
    }
    for (int i = 0; i < n; i++)
    {
        cout << q1.pop() << " ";
    }
    cout << endl;
    for (int i = 0; i < n; i++)
    {
        cout << q2.pop() << " ";
    }
    cout << endl;
    return 0;
}