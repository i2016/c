#include <iostream>
using namespace std;
class stack {
private:
    int buf[10];
protected:
    int idx;
public:
    stack() {
        idx = 0;
    }
    void push(int v) {
        buf[idx++] = v;
    }
    int pop() {
        return buf[--idx];
    }
};
class nstack : public stack {
public:
    void push(int v) {
        if(v>0) {
            stack::push(v);
        }
    }
    int isEmpty() {
        if (idx == 0) return 1;
        else return 0;
    }
};
int main() {
    nstack a; int n, v;
    cin >> n;
    for (int i = 0; i < n; i++) {
        cin >> v;
        a.push(v); 
    }
    while ( ! a.isEmpty() ) { 
        cout << a.pop() << endl;
    }
    return 0;
}