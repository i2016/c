#include <iostream>
using namespace std;

namespace Hex {
    int keta(int num) {
        int n;
        for(n = 0; num > 0; n++) {
            num /= 16;
        }
        return n;
    }
}

namespace Dec {
    int keta(int num) {
        int n;
        for(n = 0; num > 0; n++) {
            num /= 10;
        }
        return n;
    }
}

int main(void) {
    int n;
    cin >> n;
    cout << Hex::keta(n) << endl;
    cout << Dec::keta(n) << endl;
    return 0;
}