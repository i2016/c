#include <iostream>
using namespace std;

namespace Orange {
    int GetPrice() {
        return 100;
    }
}

namespace Ringo {
    int GetPrice() {
        return 50;
    }
}

int main(){
    cout << "オレンシの値段" << Orange::GetPrice() << endl;
    cout << "リンコの値段" << Ringo::GetPrice() << endl;
    return 0;
}