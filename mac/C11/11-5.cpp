#include <iostream>
#include <string>
using namespace std;

class MStack {
private:
    int idx;
    char buf[101][100]; //or [100][101]?
public:
    MStack() {
        idx = 0;
    }
    void push(char s[]) {
        strcpy(buf[idx++], s);
    }
    char *pop() {
        return buf[--idx];
    }
    bool empty() {
        return idx == 0 ? true : false;
    }
};

int main() {
    MStack s;
    char v[101];
    while(cin >> v) {
        s.push(v);
    }
    while(!s.empty()) {
        cout << s.pop() << endl;
    }
    return 0;
}