#include <iostream>
#include <string>
using namespace std;

char name[] = "クローハル変数内のフックマーク";
class Shouhin {
public:
    Shouhin() {
        strcpy(name, "メンハ変数に格納されたフックマーク");
    }
    char *getShouhin() {
        return name;
    }
private:
    char name[100];
};

int main() {
    Shouhin myShouhin;
    char name[] = "ローカル変数内のフックマーク";
    cout << ::name << endl;
    cout << myShouhin.getShouhin() << endl;
    cout << name << endl;
    return 0; 
}