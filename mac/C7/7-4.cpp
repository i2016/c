#include <iostream>
using namespace std;
int gcd(int m, int n) {
    int p=0;
    for(p=(m>n)?m:n; p >= 1; p--) {
        if(m%p==0 && n%p==0) break;
    }
    return p;
}
 
int main() {
    int a, b;
    cin >> a >> b;
    cout << gcd(a, b) << endl;
    return 0;
}