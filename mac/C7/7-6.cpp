#include <iostream>
using namespace std;

class Frac {
protected:
    int numer, denom;
    int gcd(int m, int n) {
        int p;
        for(p=(m>n)?m:n; p >= 1; p--) {
            if(m%p==0 && n%p==0) break;
        }
        return p;
    }
public:
    Frac(int shi, int bo) {
        int p = gcd(shi, bo);
        numer = shi/p;
        denom = bo/p;
    }
    virtual void print() {
        cout << "(" << numer << "/" << denom << ")" << endl;
    }
};

class Frac2: public Frac {
public:
    void print() {
        if(denom == 1) {
            cout << numer <<endl;
        } else if(numer > denom) {
            cout << numer / denom << " + (" << numer % denom <<"/" << denom << ")" << endl;
        } else {
            cout << "(" << numer << "/" << denom << ")" << endl;
        }
    }
    Frac2(int m, int n): Frac(m, n) {}
};
void func(Frac *f) {
    f -> print();
}

int main() {
    int a, b;
    cin >> a >> b;
    Frac2 f = Frac2(a, b);
    func(&f);
    return 0;
}