#include <iostream>
using namespace std;

class Date {
private:
    int year, month, day;
public:
    Date(int yy, int mm, int dd) {
        year = yy;
        month = mm;
        day = dd;
    }
    void print() {
        cout << year << "-";
        cout.width(2);
        cout.fill('0');
        cout << month << "-";
        cout.width(2);
        cout.fill('0');
        cout << day << endl;
    }
    bool equal(Date a) {
        if (year == a.year && month == a.month && day == a.day) {
            return true;
        }
        return false;
    }
};

int main() {
    int yy, mm, dd;
    cin >> yy >> mm >> dd;
    Date a(yy, mm, dd);
    a.print();
    cin >> yy >> mm >> dd;
    Date b(yy, mm, dd);
    b.print();
    if (a.equal(b)) {
        cout << "同してす" << endl; 
    } else {
        cout << "違います" << endl;
    }
    return 0; 
}