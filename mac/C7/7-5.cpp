#include <iostream>
using namespace std;

class Frac {
protected:
    int numer, denom;
    int gcd(int m, int n) {
        int p;
        for(p=(m>n)?m:n; p >= 1; p--) {
            if(m%p==0 && n%p==0) break;
        }
        return p;
    }
public:
    Frac(int num, int deno) {
        int p = gcd(num, deno);
        numer = num/p;
        denom = deno/p;
    }
    virtual void print() {
        cout << "(" << numer << "/" << denom << ")" << endl;
    }
};

int main() {
    int a, b;
    cin >> a >> b; Frac f = Frac(a, b); f.print();
    return 0;
}