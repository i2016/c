#include <iostream>
using namespace std;

class MC {
protected:
    double height;
public:
    void setHeight(double h) { height = h; }
    double SBW() {
        return 22 * height * height;
    }
};

class MC2: public MC {
public:
    double SBW() {
        return 130*height*height*height/10;
    }
};
int main() {
    double h;
    cin >> h;
    MC2 mc2;
    mc2.setHeight(h);
    MC  *p1 = &mc2;
    MC2 *p2 = &mc2;
    cout << p1->SBW() << endl;
    cout << p2->SBW() << endl;
    return 0;
}