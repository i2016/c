#include <iostream>
using namespace std;

struct stack {
    int idx;
    int buf[10];
};
struct stack empty(struct stack a) {
    a.idx = 0;
    return a;
}
struct stack push(struct stack a, int v) {
    a.buf[a.idx++] = v;
    return a;
}
struct stack pop(struct stack a) {
    a.idx--;
    return a;
}
int top(struct stack a) {
    return a.buf[a.idx - 1];
}

int main() {
    struct stack a = {0, { }};
    struct stack b = {0, { }};
    int n, v;
    cin >> n;
    a = empty(a);
    b = empty(b);
    for (int i = 0; i < n; i++) {
        cin >> v;
        a = push(a, v);
        cin >> v;
        b = push(b, v);
    }
    for (int i = 0; i < n; i++) {
        cout << top(a) << endl;
        cout << top(b) << endl;
        a = pop(a);
        b = pop(b);
    }
    return 0;
}