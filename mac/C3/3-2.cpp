#include <iostream>
using namespace std;
class stack {
        int idx;
        int buf[10];
public:
    void empty() {
        idx = 0;
    }
    void push(int v) {
        buf[idx++] = v;
    }
    int pop() {
        return buf[--idx];
    }
};
int main() {
    stack a, b;
    int n, v;
    cin >> n;
    a.empty();
    b.empty();
    for (int i = 0; i < n; i++) {
        cin >> v;
        a.push(v);
        cin >> v;
        b.push(v);
    }
    for (int i = 0; i < n; i++) {
        cout << a.pop() << endl;
        cout << b.pop() << endl;
    }
    return 0;
}