#include <iostream>
using namespace std;
class BallCount {
private:
    int b;
    int s;
    int o;
public:
    void playball() {
        b = s = o =0;
    }
    void ball() {
        b++;
        if(b == 4) {
            s = 0;
            b = 0;
        }
    }
    int strike() {
        s++;
        if(s == 3) {
            b++;
            s = 0;
            b = 0;
            o++;
            if(o == 3) {
                return 1;
            }
        }
        return 0;
    } 
    int out() {
        o++;
        if (o == 3) {
            return 1;
        } else {
            b = 0;
            s = 0;
            return 0;
        }
    }
    void hit() {
        b = s =0;
    }
    void print() {
        cout << "B: ";
        for(int i=0; i<b; i++) { cout << "●"; }
        cout <<endl;
        cout << "S: ";
        for(int i=0; i<s; i++) { cout << "●"; }
        cout <<endl;
        cout << "O: ";
        for(int i=0; i<o; i++) { cout << "●"; }
        cout <<endl;
    }
};
int main(void) {
    string s;
    BallCount sb;
    sb.playball();
    sb.print();
    while ( cin >> s ) {
        if (s == "b") { 
            sb.ball();
        } else if (s == "s") {
            if (sb.strike() == 1) {
                break;
            }
        } else if (s == "o") {
            if (sb.out() == 1) {
                break;
            }
        } else if (s == "h") {
            sb.hit();
        }
        sb.print();
    }
    return 0; 
}