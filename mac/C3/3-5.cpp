#include <iostream>
using namespace std;
class MovingObject {
private:
    int x;
    int y;
    int dir;
public:
    void init() {
        x = 0;
        y = 0;
        dir = 0;
    }
    void step() {
        switch(dir) {
            case 0: y += 80; break;
            case 1: x -= 80; break;
            case 2: x += 80; break;
            case 3: y -= 80; break;   
        }
    }
    void turnRight() {
        switch(dir) {
            case 0: dir = 2; break;
            case 1: dir = 0; break;
            case 2: dir = 3; break;
            case 3: dir = 1; break;
        }
    }
    void turnLeft() {
        switch(dir) {
            case 0: dir = 1; break;
            case 1: dir = 3; break;
            case 2: dir = 0; break;
            case 3: dir = 2; break;
        }
    }
    void get(int &x1, int &y1) {
        x1 = x;
        y1 = y;
    }
};
int main() {
    MovingObject a;
    int v;
    a.init();
    while (1) {
        cin >> v;
        if (v==0) {
            break;
        }
        switch (v) {
            case 1: a.turnLeft(); break;
            case 2: a.step(); break;
            case 3: a.turnRight(); break;
        }
    }
    int x, y;
    a.get(x, y);
    cout << "(" << x << "," << y << ")" << endl;
    return 0;
}