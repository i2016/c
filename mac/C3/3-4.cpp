#include <iostream>
using namespace std;
class Date {
private:
    int year;
    int month;
    int day;
public:
    int set(int y, int m, int d) {
        int cm[] = {0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
        if (!(1 <= m && m <= 12)) {
            return 1;
        }
        if (!(1 <= d && d <= cm[m])) {
            return 1;
        }
        year = y;
        month = m;
        day = d;
        return 0;
    }
    void get(int &y, int &m, int &d) {
        y = year;
        m = month;
        d = day;
    }
    void next() {
        int cm[] = {0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
        day++;
        if (cm[month] < day) {
            day = 1;
            month++;
            if (12 < month) {
                month = 1;
                year++;
            }
        }
    }
};


int main() {
    Date d1;
    int yy, mm, dd, y, m, d;
    cin >> yy >> mm >> dd;
    if (d1.set(yy, mm, dd) == 0) {
        d1.get(y, m, d);
        cout << yy << "年" << mm << "月" << dd << "日か設定されました" << endl; d1.next();
        d1.get(yy, mm, dd);
        cout << "次の日は" << yy << "年" << mm << "月" << dd << "日てす" << endl;
    } else {
        cout << "正しく設定されていません" << endl;
    }
    return 0;
}
