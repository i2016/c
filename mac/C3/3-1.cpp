#include <iostream>
using namespace std;
struct Date { 
    int year;
    int month;
    int day;
};
int main(void) {
    struct Date a, d;
    a.year = 2006;
    a.month = 8;
    a.day = 31;
    d = a;
    if (d.year == 2006 && d.month == 8  && d.day == 31) {
        cout << "正しい" << endl;
    } else {
        cout << "間違い" << endl;
    }
    return 0;
}