#include <iostream>

using namespace std;

class Date {
private:
	int year;
	int month;
	int day;
public:
	int set(int year, int month, int day) {
		this->year = year;
		if(0 < month && month < 13) {
			this->month = month;
			if(month == 4 || month == 6 || month == 9 || month == 11) {
				if(0< day && day < 31) {
					this->day = day;
					return 0;
				}
			} else if(month == 2) {
				if(0 < day && day < 29) {
					this->day = day;
					return 0;
				}
			} else if(0 < day && day < 32) {
				this->day = day;
				return 0;
			}
		}
		return 1;
	}
	
	void get(int* year, int* month, int* day) {
		*year = this->year;
		*month = this->month;
		*day = this->day;
	}
	
	void next(void){
		if(month == 12 && day == 31) {
			year++;
			month = 1;
			day = 1;
			return;
		}
		if(month == 4 || month == 6 || month == 9 || month == 11) {
			if(day == 30) {
				month++;
				day = 1;
				return;
			}
		} else if(month == 2) {
			if(day == 28) { 
				month++;
				day = 1;
				return;
			}
		} else if(day == 31) {
			month++;
			day = 1;
			return;
		}
		day++;
	}
};

int main(void) {
	Date d1;
	int yy, mm, dd, y = 0, m = 0, d = 0;
	cin >> yy >> mm >> dd;
	if(d1.set(yy, mm, dd) == 0) {
		d1.get(&y, &m, &d);
		cout << y << "年" << m << "月" << d << "日が設定されました" << endl;
	d1.next();
	d1.get(&y, &m, &d);
		cout << "次の日は" << y << "年" << m << "月" << d << "日です" << endl;
	} else {
		cout << "正しく設定されていません" << endl;
	}		
	return 0;
}