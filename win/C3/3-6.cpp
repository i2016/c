#include <iostream>
#include <string>
using namespace std;

class BallCount {
private:
	int b;	//ボールを数える
	int s;	//ストライクを数える
	int o;	//アウトを数える
public:
	void playball() {	//試合開始(init)
		s = 0;
		b = 0;
		o = 0;
	}

	void ball() {	//ボールカウントを増やす
		b++;
		if(b == 4) {
			s++;
			b = 0;
		}
	}
	
	int strike() {	//ストライクカウントを増やす
		s++;		
		if(s == 3) {
			s = 0;
			b = 0;
			o++;
			if(o == 3) {
				o = 0;
				return 1;
			}
		}
		return 0;
	}

	int out() {	//アウトカウントを増やす
		o++;
		b = 0;
		s = 0;
		if(o == 3) {
			o = 0;
			return 1;
		}
		return 0;
	}

	void hit() {	//ヒット
		s = 0;
		b = 0;
	}

	void print() {	//現在の状態を表示する
		cout << "B: ";
		for(int i = 0; i < b; i++)
			cout << "●";
		cout << endl;
		cout << "S: ";
		for(int i = 0; i < s; i++)
			cout << "●";
		cout << endl;
		cout << "O: ";
		for(int i = 0; i < o; i++)
			cout << "●";
		cout << endl;
	}
};	

int main(void) {
	string s;
	BallCount sb;

	sb.playball();
	sb.print();
	while( cin >> s ) {
		if(s =="b") {
			sb.ball();
		} else if(s == "s") {
			if(sb.strike() == 1) {
				break;
			}
		} else if(s == "o") {
			if(sb.out() == 1) {
				break;
			}
		} else if(s == "h") {
			sb.hit();
		}
		sb.print();
	}
	return 0;
}