#include <iostream>
#include <cmath>

using namespace std;

double menseki(double r){
	return r * r * 3.14;
}

double menseki(double w, double h){
	return w * h;
}

double menseki(double a, double b, double c){
	double s = (a + b + c) / 2;
	return sqrt(s * (s - a) * (s - b) * (s - c));
}

int main(void){
	double r;
	cin >> r;
	cout << menseki(r) << endl;

	double tate, yoko;
	cin >> tate >> yoko;
	cout << menseki(tate, yoko) << endl;

	double a, b, c;
	cin >> a >> b >> c;
	cout << menseki(a, b, c) << endl;

	return 0;
}