#include <iostream>

using namespace std;

class que{
private:
	int buf[10];
protected:
	int idx;
public:
	que(){
		idx = 0;
	}
	void enq(int v){
		buf[idx++] = v;
	}
	int deq(void){
		int ret = buf[0];
		for(int i = 0; i < idx; i++)
			buf[i] = buf[i + 1];
		idx--;
		return ret;
	}
};

class que2 : public que{
public:
	void swap(void){
		if(idx < 2)
			return;
		int tmp_buf[10];
		int tmp1 = deq();
		int tmp2 = deq();
		int i = 0;
		while(idx > 0)
			tmp_buf[i++] = deq();
		enq(tmp2);
		enq(tmp1);
		int j = 0;
		while(i > 0){
			enq(tmp_buf[j++]);
			--i;
		}
	}
};	

int main(void){
	que2 a;
	int n, v;

	cin >> n;
	for(int i = 0; i < n; i++){
		cin >> v;
		a.enq(v);
	}
	a.swap();
	for(int i = 0; i < n; i++){
		cout << a.deq() << " ";
	}
	cout << endl;
	return 0;
}