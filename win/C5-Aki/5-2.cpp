#include <iostream>

using namespace std;

class stack{
private:
	int idx;
	int buf[10];
public:
	stack(void) {
		idx = 0;
	}
	void push(int v) {
		buf[idx++] = v;
	}
	int pop() {
		return buf[--idx];
	}
};

class stack2 : public stack{
public:
	void swap(void){
		int v1 = pop();
		int v2 = pop();
		push(v1);
		push(v2);
	}
};

int main(void){
	stack2 a;
	int n, v;

	cin >> n;
	for (int i = 0; i < n; i++) {
		cin >> v;
		a.push(v);
	}
	a.swap();
	for (int i = 0; i < n; i++) {
		cout << a.pop() << endl;
	}
	return 0;
}